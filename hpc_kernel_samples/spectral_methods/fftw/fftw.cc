
// =================================================================================================
// This file is part of the CodeVault project. The project is licensed under Apache Version 2.0.
// CodeVault is part of the EU-project PRACE-4IP (WP7.3.C).
//
// Author(s):
//   Cedric Nugteren <cedric.nugteren@surfsara.nl>
//
// This example demonstrates the use of a fast Fourier transform library for CPUs: FFTW3. 
// The example is set-up to perform an in-place forward transform followed by an in-place inverse
// transform such that the output signal should be equal to the input signal. The example uses pre-
// processor defines to select the FFT-size and the number of batches. The FFTs performed are 1D
// and double-precision complex-to-complex.
//
// See [http://www.fftw.org/fftw3_doc/] for the full FFTW3 documentation.
//
// =================================================================================================

#include <cstdio>
#include <cstdlib>
#include <cmath>

// The FFTW3 library
#include <fftw3.h>

// Constants
#define NAME "[3_spectral_fftw] "

// Settings
#define SIZE 128           // The size of the FFT to perform
#define BATCHES 20         // The number of batches of FFTs of size 'SIZE'
#define ERROR_MARGIN 1e-8  // Acceptable absolute error for validation

// =================================================================================================

// Monolithic function without command-line arguments
int main() {
  printf("\n");

  // Allocates and initializes example data. This creates two arrays with the same (random)
  // contents: one to be processed (read/write) and one for verification purposes. Note that this
  // uses special 'fftw_malloc' calls to make sure memory is aligned correctly to enable use of SIMD
  // instructions (e.g. SSE/AVX/NEON). The 'fftw_complex' data-type is implemented as an array of
  // two doubles.
  fftw_complex* example_data = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*SIZE*BATCHES);
  fftw_complex* signal_host = (fftw_complex*) fftw_malloc(sizeof(fftw_complex)*SIZE*BATCHES);
  for (int i=0; i<SIZE*BATCHES; ++i) {
    example_data[i][0] = rand() / ((double)RAND_MAX);
    example_data[i][1] = rand() / ((double)RAND_MAX);
    signal_host[i][0] = example_data[i][0];
    signal_host[i][1] = example_data[i][1];
  }

  // Creates the FFTW3 plans. This describes the size and type of FFTs to be performed. The given
  // arguments are:
  //   1) the rank or dimensionality (1D, 2D, or 3D)
  //   2) the FFT-sizes in each dimension (e.g. 128-points transform)
  //   3) the number of batches
  //   4-7) the input signal and its characteristics such as strides (not used in this example)
  //   8-11) as above, but now for the output
  //   12) the direction (forward or inverse transform)
  //   13) other flags: here FFTW_ESTIMATE is given for a relatively fast plan creation
  // More information available online: [http://www.fftw.org/doc/Advanced-Complex-DFTs.html]
  printf("%sCreating FFTW3 plans\n", NAME);
  int sizes[] = {SIZE};
  fftw_plan plan1 = fftw_plan_many_dft(1, sizes, BATCHES,
                                       signal_host, NULL, 1, SIZE,
                                       signal_host, NULL, 1, SIZE,
                                       FFTW_FORWARD, FFTW_ESTIMATE);
  fftw_plan plan2 = fftw_plan_many_dft(1, sizes, BATCHES,
                                       signal_host, NULL, 1, SIZE,
                                       signal_host, NULL, 1, SIZE,
                                       FFTW_BACKWARD, FFTW_ESTIMATE);

  // Performs the forward FFTW based on the created plan. Nothing has to be done here, since all
  // arguments were already given at plan-creation time.
  printf("%sPerforming a 1D forward FFT using FFTW3\n", NAME);
  fftw_execute(plan1);

  // As above, but now performs an inverse FFT to 'undo' the previous FFT (allows for easier
  // validation).
  printf("%sPerforming a 1D inverse FFT using FFTW3\n", NAME);
  fftw_execute(plan2);

  // Divides the result by the signal size to obtain the original input signal again. Since FFTW3
  // does not normalize the signal, this is done on the host.
  for (int i=0; i<SIZE*BATCHES; ++i) {
    signal_host[i][0] = signal_host[i][0] / ((double)SIZE);
    signal_host[i][1] = signal_host[i][1] / ((double)SIZE);
  }

  // Iterates over all signal elements and verifies the resulting signal against the original
  // input. There might be some rounding errors, but they should be smaller than the absolute error
  // margin.
  int num_errors = 0;
  for (int i=0; i<SIZE*BATCHES; ++i) {
    if (fabs(example_data[i][0] - signal_host[i][0]) > ERROR_MARGIN ||
        fabs(example_data[i][1] - signal_host[i][1]) > ERROR_MARGIN) {
      num_errors++;
    }
  }
  printf("%sValidation: %d out of %d correct\n", NAME, SIZE*BATCHES - num_errors, SIZE*BATCHES);

  // Cleans-up the memory and FFTW3 plans
  printf("%sAll done\n", NAME);
  fftw_destroy_plan(plan1);
  fftw_destroy_plan(plan2);
  fftw_free(signal_host);
  fftw_free(example_data);

  // End of the code example
  printf("\n");
  return 0;
}

// =================================================================================================
