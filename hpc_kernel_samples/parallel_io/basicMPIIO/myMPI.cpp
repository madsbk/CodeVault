#include "myMPI.hpp"

namespace my
{
  template<>
    size_t MPI::Comm::allreduce<size_t>(size_t value, MPI::Op op) const
    {
      auto src  = static_cast<long long>(value);
      auto dst = src;
      MPI_Allreduce(&src, &dst, 1, MPI_LONG_LONG, decode_op(op), comm_);
      return static_cast<size_t>(dst);
    }
  template<>
    size_t MPI::Comm::reduce<size_t>(size_t value, MPI::Op op, int root) const
    {
      auto src  = static_cast<long long>(value);
      auto dst = src;
      MPI_Reduce(&src, &dst, 1, MPI_LONG_LONG, decode_op(op), root, comm_);
      return static_cast<size_t>(dst);
    }
  template<>
    void MPI::Comm::allgather<size_t>(size_t value, size_t *data) const
    {
      auto value_ll = static_cast<long long>(value);
      auto data_ll  = reinterpret_cast<long long*>(data);
      MPI_Allgather(&value_ll,1, MPI_LONG_LONG, data_ll, 1, MPI_LONG_LONG, comm_);
    }
}
