cmake_minimum_required(VERSION 2.8.10 FATAL_ERROR)

# Packages are optional: if they are not present, certain code samples are not compiled
find_package(OpenMP)   # Built-in in CMake

include(${CMAKE_CURRENT_SOURCE_DIR}/../../../cmake/common.cmake)

# ==================================================================================================

if ("${DWARF_PREFIX}" STREQUAL "")
  set(DWARF_PREFIX 7_STRUCT)
endif()
set(NAME ${DWARF_PREFIX}_openmp)

# ==================================================================================================
# C++ compiler settings

find_package(Common)
select_compiler_flags(c_flags
  GNU "-std=c99 -DUSE_OPENMP"
  Intel "-restrict")   # "-O3" is controlled by the CMAKE_BUILD_TYPE
set(C_FLAGS ${c_flags})
if("${CMAKE_C_COMPILER_ID}" STREQUAL "GNU")
  set(C_FLAGS "${C_FLAGS} -Wall -Wno-comment")
endif()
set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${C_FLAGS}")
link_libraries(m)
if (OPENMP_FOUND)
    set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${OpenMP_C_FLAGS}")
    add_executable (${NAME} src/kernels_c.c src/comm_mpi_c.c src/utils_c.c src/jacobi_c.c)
    install(TARGETS ${NAME} DESTINATION bin)
    message("** Enabling '${NAME}': with OpenMP")
else()
     message("## Skipping '${NAME}_omp': no OpenMP support found")
     dummy_install(${NAME} "OpenMP")
endif()

unset(NAME)

# ==================================================================================================