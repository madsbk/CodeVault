#ifndef PARTICLES_H
#define PARTICLES_H

#include "vector.h"

typedef struct
{
   int count;
   int capacity;

   int *owner_rank;
   double *mass;
   vec_t force;
   vec_t velocity;
   vec_t location;
} part_t;

void part_init(part_t *particles, int capacity);
void part_reserve(part_t *particles, int capacity);
void part_resize(part_t *particles, int count);
void part_free(part_t *particles);

void part_copy(part_t *dst, int dst_start, const part_t *src, int src_start, int count);

#endif

