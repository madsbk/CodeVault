#ifndef CONFIGURATION_H
#define CONFIGURATION_H

#include <stdio.h>

enum verbosity_level_enum
{
   OFF = 0,
   INFO = 1,
   DEBUG = 2,
   TRACE = 3
};

enum transmission_mode_enum
{
   DYNAMIC = 0,
   COLLECTIVE = 1,
   RMA = 2
};

typedef struct
{
   int verbosity_level;
   int debug_rank;
   int use_cartesian_topology;
   int transmission_mode;

   int ncells_x;
   int ncells_y;
   int particles_per_cell_initial;

   int n_iterations;
   double delta_t;
   double min_mass;
   double max_mass;
   double max_force;
   double max_velocity_initial;
} conf_t;

// Meta-information for MPI-Datatype creation (see mpitypes.c)
#define CONF_T_N_INT_MEMBERS 8
#define CONF_T_FIRST_INT_MEMBER verbosity_level
#define CONF_T_N_DOUBLE_MEMBERS 5
#define CONF_T_FIRST_DOUBLE_MEMBER delta_t

void conf_init(conf_t *c);

const char* conf_set_from_args(conf_t *c, int argc, char* argv[], int nprocs);

void conf_print(const conf_t *c, FILE *file);

int info_enabled(const conf_t *c);
int warn_enabled(const conf_t *c);
int debug_enabled(const conf_t *c);
int trace_enabled(const conf_t *c);

#endif

