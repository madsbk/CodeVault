# Packages are optional: if they are not present, certain code samples are not compiled
cmake_minimum_required(VERSION 2.8.10 FATAL_ERROR)

find_package(MPI)      # Built-in in CMake

include(${CMAKE_CURRENT_SOURCE_DIR}/../../cmake/common.cmake)

# ==================================================================================================

if ("${DWARF_PREFIX}" STREQUAL "")
  set(DWARF_PREFIX 8_unstructured)
endif()

set(NAME ${DWARF_PREFIX}_halo_exchange)

# C compiler settings

find_package(Common)

if (MPI_FOUND)
    cmake_policy(SET CMP0003 OLD)
    set (CMAKE_C_FLAGS "${CMAKE_C_FLAGS}")
    include_directories(${MPI_INCLUDE_PATH})
    add_executable(${NAME} main.c configuration.c mpicomm.c mpitypes.c box.c field.c mesh.c)
    target_link_libraries(${NAME} ${MPI_LIBRARIES} stdc++)
    install(TARGETS ${NAME} DESTINATION bin)
    message("** Enabling '${NAME}': with MPI")
else()
    message("## Skipping '${NAME}': no MPI support found")
#    dummy_install(${NAME} "MPI")
endif()

set(CMAKE_C_FLAGS "${CMAKE_C_FLAGS} ${C_FLAGS}")

unset(NAME)
# ==================================================================================================
