/*includes {{{*/
#include <stdio.h>
#include <assert.h>
#include <math.h>
#include <mkl.h>
#define MIC_TARGET
#include "../../common/mmio.h"
#include "../../common/util.h"
/*}}}*/

/* global varibles and defines {{{*/
int nrepeat = 100;
char transa = 'n';
/*}}}*/

/** Multiply two sparse matrices which are stored in CSR format. MKL is used */
void mkl_cpu_spmv(const MKL_INT Am, const MKL_INT An,  double* Aval, MKL_INT* AJ, MKL_INT* AI, double* xvec, double* yvec, double* time) {  /*{{{*/
    MKL_INT ierr;
    MKL_INT request = 1;    // symbolic multiplication
    double alpha = 1.0;
    double beta = 0.0;
    char* matdescra = "G NF";

    double time_st = dsecnd();
    int i;
    for(i = 0; i < nrepeat; i++) {
        /** y := alpha*A*x + beta*y */
        mkl_dcsrmv(&transa,  &Am, &An, &alpha, matdescra, Aval, AJ, AI, (AI+1), xvec, &beta, yvec);
    }
    double time_end = dsecnd();
    *time = (time_end - time_st)/nrepeat;
} /*}}}*/

int main(int argc, char* argv[]) { /*{{{*/
    /** usage */
    int nrequired_args = 4;
    if (argc != nrequired_args){
        fprintf(stderr, "NAME:\n\tmkl_spmv - multiply a sparse matrix with a dense vector\n");
        fprintf(stderr, "\nSYNOPSIS:\n");
        fprintf(stderr, "\tmkl_spmv MATRIX_A NUMBER_OF_THREADS PRINT_MATRIX\n");
        fprintf(stderr, "\nDESCRIPTION:\n");
        fprintf(stderr, "\tNUMBER_OF_THREADS: {0,1,2,...}\n");
        fprintf(stderr, "\t\t0: Use number of threads determined by MKL\n");
        fprintf(stderr, "\tPRINT_MATRIX: PRINT_YES, PRINT_NO\n");
        fprintf(stderr, "\nSAMPLE EXECUTION:\n");
        fprintf(stderr, "\t%s test.mtx 2 PRINT_YES\n", argv[0]);
        exit(1);
    }
    /** parse arguments */
    int iarg = 1;
    char* strpathA = argv[iarg];    iarg++;
    int nthreads = atoi(argv[iarg]);    iarg++;
    if (nthreads > 0) {
        mkl_set_num_threads(nthreads);
    } else {
        nthreads = mkl_get_max_threads();
    }
    option_print_matrices = strcmp(argv[iarg], "PRINT_YES")==0?OPTION_PRINT_MATRICES:OPTION_NOPRINT_MATRICES;    iarg++;
    assert(nrequired_args == iarg);

    /** read matrix market file for A matrix */
    MKL_INT Am, An, Annz;
    MKL_INT *Ax, *Ay;
    double *Anz;
    read_mm(strpathA, &Am, &An, &Annz, &Ax, &Ay, &Anz);

    /** construct csr storage for A matrix */
    MKL_INT* AJ = (MKL_INT*) mkl_malloc( Annz * sizeof( MKL_INT ), 64 );
    MKL_INT* AI = (MKL_INT*) mkl_malloc( (Am+1) * sizeof( MKL_INT ), 64 );
    double* Aval = (double*) mkl_malloc( Annz * sizeof( double ),  64 );
    coo_to_csr(Am, Annz, Ax, Ay, Anz, AI, AJ, Aval);

    double* xvec = (double*) mkl_malloc( An * sizeof( double ), 64 );
    double* yvec = (double*) mkl_malloc( Am * sizeof( double ), 64 );
    int i;
    for(i=0;i<An;i++)xvec[i]=1.0;
    for(i=0;i<Am;i++)yvec[i]=0.0;

    double time;
    mkl_cpu_spmv(Am, An, Aval, AJ, AI, xvec, yvec, &time);

    printmm_one(Am, Aval, AJ, AI);

    /** number of multiply-and-add operations in terms of giga flops*/
    double gflop = 2 * (double) Annz / 1e9;

    /** print gflop per second and time */
    printf("%d\t", nthreads);
    printf("%g\t", (gflop/time));
    printf("%g\t", time);
    printf("%s\t", strpathA);
    printf("\n");

    printf("Output vector:\n");
    for(i=0;i<Am;i++)printf("%g ", yvec[i]);
    printf("\n");
    /** free allocated space */
    mkl_free(AI);
    mkl_free(AJ);
    mkl_free(Aval);
    return 0;
} /* ENDOF main }}}*/


