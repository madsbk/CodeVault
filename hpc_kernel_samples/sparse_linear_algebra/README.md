CodeVault: Sparse Linear Algebra
================
# Overview
In this Sparse Linear Algebra folder, there are sample codes for beginners, as well as advanced codes including important computational kernels.

# Contributors & Maintainers
- Cevdet Aykanat (aykanat@cs.bilkent.edu.tr) 
- Kadir Akbudak (kadir.cs@gmail.com) 
- Reha Oguz Selvitopi(reha@cs.bilkent.edu.tr) 

# Contents
- spmv: Multiplication of a sparse matrix with a dense vector.
  - mkl_shmem: Using MKL's routine mkl_dcsrmv() on a multicore processor
- spgemm: Multiplication of two sparse matrices.
  - mkl_shmem: Using MKL's routine mkl_dcsrmultcsr() on a multicore processor
  - mkl_xphi: Using MKL's routine mkl_dcsrmultcsr() via offloading to a Xeon Phi coprocessor
  These two codes also contain schoolbook implementation of an SpGEMM algorithm that uses row-by-row formulation [1].
- Krylov Subspace Methods 
  - Linear system  solution  in parallel 
    - 2D Laplacian (2D mesh) 
  - Repeatedly  solving  two linear systems 
    - Same preconditioner 
    - Two different matrices with the same nonzero pattern 
  - Solving  multiple  linear systems 
    - Same cofficient matrix 
    - Different right-hand-side vectors 


REFERENCES:
[1] Fred G. Gustavson. 1978. Two Fast Algorithms for Sparse Matrices: Multiplication and Permuted Transposition. ACM Trans. Math. Softw. 4, 3 (September 1978), 250-269. DOI=http://dx.doi.org/10.1145/355791.355796 
